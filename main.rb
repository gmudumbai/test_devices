require './helpers'
require 'sinatra/base'

class MyApp < Sinatra::Base
  helpers ApplicationHelper
  
  def initialize
    super()
    
    load_devices
    load_testers
    load_bugs
    load_tester_devices
    
    puts "Loaded CSV data"
  end
  
  get '/' do
    content_type :json 
    
    users = get_sorted_users(params)    
    
    users.to_json
  end
end
