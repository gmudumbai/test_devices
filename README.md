# Test Devices

$ docker build --tag app .

$ docker run -p 80:4567 app

$ open http://localhost?country=ALL&device=iPhone%204,iPhone%205

$ open http://localhost?country=GB,US&device=iPhone%204,Nexus%204

$ open http://localhost?country=GB,US

$ open http://localhost?country=all

$ open http://localhost?device=iphone%204

$ open http://localhost


EXAMPLE OUTPUT: 
**[["Stanley Chen",110],["Taybin Rutkin",66],["Sean Wellington",58],["Miguel Bautista",53],["Leonard Sutton",32],["Mingquan Zheng",21]]**
