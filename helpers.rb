require 'csv'

module ApplicationHelper
  def load_devices
    @devices = CSV.foreach('./data/devices.csv', {headers: true}).to_a.map{|x| x.to_hash}
  end
  
  def load_testers
    @testers = CSV.foreach('./data/testers.csv', {headers: true}).to_a.map{|x| x.to_hash}
  end

  def load_bugs
    @bugs = CSV.foreach('./data/bugs.csv', {headers: true}).to_a.map{|x| x.to_hash}
  end
  
  def load_tester_devices
    @tester_devices = CSV.foreach('./data/tester_device.csv', {headers: true}).to_a.map{|x| x.to_hash}
  end
  
  def get_sorted_users(params)
    country_filter = get_country_filter(params['country'].to_s.downcase)
    device_filter = get_device_filter(params['device'].to_s.downcase)
    
    device_ids = get_device_ids_by_filter(device_filter)
    puts "Device IDs: #{device_ids.inspect}"
    
    tester_ids_by_country = get_tester_ids_by_country(country_filter)
    puts "Tester IDs by country: #{tester_ids_by_country.inspect}"
    
    tester_ids_by_device = get_tester_ids_by_device(device_ids)
    puts "Tester IDs by device: #{tester_ids_by_device}"
    
    eligible_tester_ids = get_union_of_testers(tester_ids_by_country, tester_ids_by_device)
    puts "Eligible Tester IDs: #{eligible_tester_ids}"
    
    testers_experience = get_testers_experience(eligible_tester_ids, device_ids)
    puts "Tester Experiences: #{testers_experience}"
    
    get_testers_sorted_by_experience(testers_experience)
  end
  
  def get_country_filter(country_filter)
    if country_filter.empty? || country_filter.eql?('all')
      'all'
    else
      country_filter.split(',')
    end
  end
  
  def get_device_filter(device_filter)
    if device_filter.empty? || device_filter.eql?('all')
      'all'
    else
      device_filter.split(',')
    end
  end
  
  def get_device_ids_by_filter(filter)
    if filter.eql? 'all'
      # SELECT deviceId FROM devices
      @devices.map{|d| d['deviceId']}
    else
      # SELECT deviceId FROM devices WHERE description IN (filter)
      @devices.select{|device| filter.include?(device['description'].downcase)}.map{|d| d['deviceId']}
    end    
  end
  
  def get_tester_ids_by_country(filter)
    if filter.eql? 'all'
      # SELECT testerId FROM testers
      @testers.map{|t| t['testerId']}
    else
      # SELECT testerId FROM testers WHERE country IN (filter)
      @testers.select{|tester| filter.include?(tester['country'].downcase)}.map{|t| t['testerId']}
    end
  end
  
  def get_tester_ids_by_device(devices)
    # SELECT DISTINCT testerId FROM tester_devices WHERE deviceId IN (devices)
    @tester_devices.select{|td| devices.include?(td['deviceId'])}.map{|t| t['testerId']}.uniq
  end
  
  def get_union_of_testers(tester_ids_1, tester_ids_2)
    # SELECT DISTINCT t.testerId FROM testers t, tester_devices td, devices d
    # WHERE t.testerId = td.testerId AND d.deviceId = td.deviceId
    # AND d.device IN (devices) AND t.country IN (countries)
    tester_ids_1 & tester_ids_2
  end
    
  
# SELECT testerId, (t.firstName + ' ' + t.lastName) AS Name, COUNT(0) AS Experience
# FROM testers t 
# JOIN tester_devices td
  # ON t.testerId = td.testerId
# JOIN devices d
  # ON d.deviceId = td.deviceId
# JOIN bugs b
  # ON d.deviceId = b.deviceId AND t.testerId = b.testerId
# WHERE d.devices IN <devices>
# AND t.country IN <countries>
# GROUP BY b.testerId, (t.firstName + ' ' + t.lastName)
# ORDER BY Experience DESC
  def get_testers_experience(tester_ids, device_ids)
    testers_hash = {}
    
    tester_ids.each do |tester_id|
      tester = @testers.find{|t| t['testerId'].eql?(tester_id)}
      tester_name = "#{tester['firstName']} #{tester['lastName']}"
      bugs_count = @bugs.count {|bug| bug['testerId'].eql?(tester_id) && device_ids.include?(bug['deviceId'])}
      
      testers_hash[tester_name] = bugs_count
    end
    
    testers_hash
  end
  
  def get_testers_sorted_by_experience(testers_experience)
    testers_experience.sort_by {|k, v| -v}
  end
end
